// 创建一个全局数据对象
export const tabbarlist1 = [
    {
    "pagePath": "/pages/index/index",
    "label": "首页",
    "iconName": 'icon-shouye1'
}, {
    "pagePath": "/pages/message/index",
    "label": "消息",
    "iconName": 'icon-xinxi2'
},

    {
        "pagePath": "/pages/square/index",
        "label": "广场",
        "iconName": 'icon-guangchang'
    },
    {
        "pagePath": "/pages/pack/index",
        "label": "礼包",
        "iconName": 'icon-libao'
    },
    {
        "pagePath": "/pages/mine/index",
        "label": "我的",
        "iconName": 'icon-wode'
    }
]
export const tabbarlist2 = [
    {
        "pagePath": "/pages/business/index",
        "label": "首页",
        "iconName": 'icon-shouye1'
    }, {
        "pagePath": "/pages/business/order",
        "label": "订单",
        "iconName": 'icon-neirong'
    },
    {
        "pagePath": "/pages/business/liveBroadcast",
        "label": "直播",
        "iconName": 'icon-zhanwaituiguangguanggaoshuju'
    },
    {
        "pagePath": "/pages/business/popularize",
        "label": "推广",
        "iconName": 'icon-tuiguang1'
    },
    {
        "pagePath": "/pages/business/mine",
        "label": "我的",
        "iconName": 'icon-wode'
    }
]