import {
	baseUrl
} from './config.js';
/**
 * @param{string}url 请求地址(baseUrl基础地址之后的地址)
 * @param{Object}data 请求参数
 * @param{string}method 请求方法(默认为GET请求)
 * @param{string}contentType 请求内容类型(默认为JSON类型，可选form)
 * @param{Object}headers 其他 header 数据
 */
export default (url, data, method = 'GET', contentType = 'form', headers = {
	versionSy: uni.getStorageSync('versionSy'),
	version: uni.getStorageSync('version')
}) => {
	// 判断url地址第一个字符是否含 '/', 有则截取掉
	if (baseUrl.toString().slice(baseUrl.length - 1) === '/' && url.toString().slice(0, 1) === '/') url = url.toString()
		.slice(1)
	// 删除data对象空属性
	for (let i in data) {
		if (typeof(data[i]) == 'undefined' || (data[i] != 0 && !data[i])) {
			delete data[i]
		}
	}
	// 判断请求内容类型
	if (!(/[A-Za-z]{4}/.test(contentType))) throw 'contentType 格式不正确，请检查是否为字符串json或者form'
	if (contentType.toUpperCase() === 'JSON') {
		headers['Content-Type'] = 'application/json; charset=UTF-8' // 可以传引用类型
	} else {
		headers['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8' // 无法传引用类型（传的是字符串，后端不想解析）
	}
	// headers['Referer'] = 'https://www.douyin.com/'
	// console.log(headers, 8, '请求首页')
	// 存储TOKEN
	let token = uni.getStorageSync('token')
	if (token) headers.token = token
	// 假设你想检查的页面路径是 '/pages/index/index'
	const pagePath = 'pages/login/resetpwd';
	// 获取当前页面栈
	const pages = getCurrentPages();
	// 获取栈顶页面
	const currentPage = pages[pages.length - 1];
	// 检查当前页面是否是指定页面
	const isSpecifiedPage = currentPage.route === pagePath || currentPage.route === pagePath + '.vue';
	if(!isSpecifiedPage){
		if (!token) {
			uni.redirectTo({
				url: '/pages/login/login',
			})
		}
	}



	return new Promise(function(resolve, reject) {
		uni.showLoading({
			icon: 'none',
			title: '加载中...'
		})
		uni.request({
			url: baseUrl + url,
			data: data,
			method: method,
			header: headers,
			timeout: 60000,
			success: function(res) {
				uni.hideLoading()
				if (res.data.code == 401) { // token 过期
					uni.redirectTo({
						url: '/pages/login/login',
					})
				}
				if(res.data.code == 299){
					resolve(res.data)
				}
				if (parseInt(res.statusCode) === 200) {
					if (res.data.code) {
						uni.showToast({
							title: res.data.msg,
							icon: 'none'
						})
					} else {
						resolve(res.data)
					}
				} else if (parseInt(res.statusCode) === 401) {} else if (parseInt(res
					.statusCode) === 500) {} else {}
			},
			fail: function(err) {

			}
		})
	}).finally(() => {
		uni.hideLoading()
	})
}