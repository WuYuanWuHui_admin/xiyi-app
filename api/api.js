// 请求地址
export default {
	'login': '/nt/user/login', //登录
	'updateUserPassword': '/nt/user/updateUserPassword', //重置密码
	'findMerchantList': '/nt/user/findMerchantList', //商家列表
	'getUserDeatil': '/nt/user/getUserDeatil', //用户信息
	'findMerchantGoodList': '/nt/user/findMerchantGoodList', //免费专区
	'userAuth': '/nt/user/userAuth', //用户认证
	'findGood': '/nt/user/findGood', //免费专区页面详情
	'addMerchangoodReservation': '/nt/user/addMerchangoodReservation', //立即预约免费
	'getUserAccountDeatil': '/nt/user/getUserAccountDeatil', //我的头部 
	'updateMerchantStore':'/nt/user/updateMerchantStore',//创建店铺名
	'updateMerchangood':'/nt/user/updateMerchangood',//修改商品状态
	'addMerchangood':'/nt/user/addMerchangood',//添加商品信息
	'upload':'/common/uploadFile',//上传照片
	'selsectMerchantGoodList':'/nt/user/selsectMerchantGoodList',//查询商家所有商品
	'findMerchangoodReservationlist':'/nt/user/findMerchangoodReservationlist',//用户预约列表
	'findMerchangoodReservationHXlist':'/nt/user/findMerchangoodReservationHXlist',//商户核销列表
	'merchantAuth':'/nt/user/merchantAuth',//商户认证
	'updateMerchantInfo':'/nt/user/updateMerchantInfo',//修改商户资料
	'merchangoodReservationWriteOff':'/nt/user/merchangoodReservationWriteOff',//扫码核销
	'findUserPush':'/nt/user/findUserPush',//推荐码
	'loginOut':'/nt/user/loginOut',//退出登录
	'userSignIn':'/nt/user/sgin/userSignIn',//每日签到
	'findSystemSignTaskList':'/nt/user/sgin/findSystemSignTaskList',//获取系统消费券列表
	'findUserSignTaskList':'/nt/user/sgin/findUserSignTaskList',//用户签到任务列表
	'userSignExchange':'/nt/user/sgin/userSignExchange',//礼包兑换
	'switchUserAuth':'/nt/user/switchUserAuth',//用户切换身份
	'myTeam':'/nt/user/myTeam',//我的团队
	'myTeamPush':'/nt/user/myTeamPush',//团队-我的好友
	'operateUserAccount':'/nt/account/operateUserAccount',//查看直推用户完整手机号，使用豆子 0.1
	'refreshLevel':'/nt/user/refreshLevel',//刷新星级
	'addUserDraw':'/nt/user/addUserDraw',//用户提现
	'getNewVersion':'/nt/user/app/getNewVersion',//获取最新APP版本
	'list':'/nt/user/accoundetail/list',//用户资金明细
	'drawList':'/nt/user/drawList',//余额明细
	'bankCardList':'/nt/user/pay/bank/list',//银行卡列表
	'addPayBank':'/nt/user/pay/bank/addPayBank',//添加银行卡
	'placeOrderPay':'/nt/pay/placeOrderPay',//查询支付渠道
	'myAccountList':'/api/heepay/myAccountList',//查询用户自己的汇付宝银行卡列表
	'getSignPageInfo':'/api/heepay/getSignPageInfo',//汇付宝跳转绑定银行卡页面
	'hfbSendPayOrderSms':'/nt/pay/hfbSendPayOrderSms',//汇付宝发送支付验证码短信
	'hfbConfirmPay':'/nt/pay/hfbConfirmPay',//输入验证码确认支付
	'createAgentVipOrder':'/nt/agentPay/createAgentPayVipOrder',//创建代付会员订单
	'queryAgentPayVipOrder':'/nt/agentPay/queryAgentPayVipOrder',//查询代付订单
	'hfbAgeSendPayOrderSms':'/nt/agentPay/hfbAgeSendPayOrderSms',//代付时汇付宝发送短信
	'hfbAgeConfirmPay':'/nt/agentPay/hfbAgeConfirmPay',//代付时汇付宝确认支付
	'uploadHeadImage':'/nt/user/uploadHeadImage',//上传头像
	'updateUser':'/nt/user/updateUser',//修改用户昵称，头像
	'imgVerificationCode':'/nt/verificationCode/imgVerificationCode',//获取图形验证码
	'sendSms':'/nt/verificationCode/sendSms',//发送验证码
	'smsUpdateUserPassword':'/nt/user/smsUpdateUserPassword',//重置密码
	'transferYidou':'/nt/user/app/transferYidou',//转赠亦豆接口
 	'createAgentPayVipOrder':'/nt/userSunshine/createAgentPayVipOrder',//查询阳光值获取记录
	'queryLotteryRecord':'/nt/user/Lottery/queryLotteryRecord',//查询自己的抽奖记录
	'singleLottery':'/nt/user/Lottery/singleLottery',//单次抽奖
	'batchLottery':'/nt/user/Lottery/batchLottery',//批次抽奖


}